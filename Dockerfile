FROM maven:3.8.3-openjdk-17 
COPY my-app/target/my-app-1.0-SNAPSHOT.jar .
CMD [ "java", "-cp my-app-1.0-SNAPSHOT.jar com.mycompany.app.App" ]
